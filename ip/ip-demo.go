package main

import (
	"github.com/dianjiu/gokit/ip"
	"log"
)

func main() {
	ip := ip.GetLocalIPv4()
	log.Printf("ip.GetLocalIPv4() Result: %v\n", ip)
}
